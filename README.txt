
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * How to use
 * Configuration
 * Links

INTRODUCTION
------------

hh.ru - HeadHunter.

This module provides integration with https://www.hh.ru/ service through API.
It needs the library https://github.com/seregazhuk/php-headhunter-api

REQUIREMENTS
------------

This module requires the following modules:

 * Libraries API (https://www.drupal.org/project/libraries)

INSTALLATION
------------

 * Go to /sites/all/libraries directory
   Run:
   > composer require seregazhuk/headhunter-api

It needs Composer to install all additional libraries.
Instruction about the library you can see here: https://github.com/seregazhuk/php-headhunter-api

HOW TO USE
----------

Some operations don't need the token to access to info - for example, view vacancy.
Just use hh_get_api() to get the API object. You can use it to run functions from here.

- Example:

<?php
    // View vacancy by id "123".
    $api = hh_get_api();
    $result = $api->vacancies->view(123);
?>

CONFIGURATION
-------------

Some operations (create vacancies etc.) need the private token to access.
How to get the token:

 * Register your application here - https://dev.hh.ru/
 * Open /admin/config/system/hh page and fill "Client ID" and "Client Secret" keys
 * Push the button "Login to HH" to update the token from hh.ru

The module will work with this token automatically. Just use hh_get_api() to get API object with extended access.

LINKS
-----

Official API documentation (RU/EN) - https://github.com/hhru/api/blob/master/README.md#headhunter-api
Source of HH library - https://github.com/seregazhuk/php-headhunter-api
